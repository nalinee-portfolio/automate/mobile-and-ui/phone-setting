*** Settings ***
Library    AppiumLibrary

*** Variables ***
${Remote_URL}                http://localhost:4723/wd/hub
${platformName}              Android
${platformVersion}           12.0
${deviceName}                emulator-5554
${appPackage}                com.android.settings
${appActivity}               com.android.settings.Settings
${locator_search}            id=com.android.settings:id/search_action_bar_title
${locator_inputsearch}       id=com.google.android.settings.intelligence:id/open_search_view_edit_text
${locator_network}           xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[6]/android.widget.LinearLayout/android.widget.TextView[1]
${locator_airplanemode}      id=android:id/switch_widget
${locator_back}              xpath=//android.widget.ImageButton[@content-desc="Navigate up"]
${locator_back2}             xpath=//android.widget.ImageButton[@content-desc="Back"]
${locator_battery}           xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[5]/android.widget.RelativeLayout/android.widget.TextView[1]