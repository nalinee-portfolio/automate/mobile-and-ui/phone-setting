*** Settings ***
Library        AppiumLibrary
Resource       ${CURDIR}/PhoneSetting_Keyword.robot


*** Test Cases ***
TC001-Open Airplane mode
    Open setting app
    Input keyword
    Open Airplane mode
    Click Back home
    Close Application

TC002-Open battery
    Open setting app
    Swipe    458    1735    501    881
    Click battery
    Close Application