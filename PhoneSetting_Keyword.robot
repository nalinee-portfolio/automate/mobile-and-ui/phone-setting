*** Settings ***
Library        AppiumLibrary
Resource       ${CURDIR}/PhoneSetting_Variable.robot

*** Keywords ***
Open setting app
    Open Application            ${Remote_URL}
    ...                         platformName=${platformName}    
    ...                         platformVersion=${platformVersion}    
    ...                         deviceName=${deviceName}  
    ...                         automationName=UiAutomator2    
    ...                         newCommandTimeout=2500 
    ...                         appPackage=${appPackage}    
    ...                         appActivity=${appActivity}  
    Wait Until Page Contains    Settings 

Input keyword
    Click Element                    ${locator_search}
    Wait Until Element Is Visible    ${locator_inputsearch}    5s
    input text                       ${locator_inputsearch}    Network
    Wait Until Element Is Visible    ${locator_network}        5s
    Click Element                    ${locator_network}
    Wait Until Page Contains         Airplane mode  
    
Open Airplane mode
    Wait Until Element Is Visible    ${locator_airplanemode}
    Click Element                    ${locator_airplanemode}

Click Back home
    Wait Until Element Is Visible    ${locator_back}
    Click Element                    ${locator_back}
    Wait Until Element Is Visible    ${locator_back2}
    Click Element                    ${locator_back2}

Click battery
    Wait Until Page Contains        Battery
    Click Element                   ${locator_battery}
    Wait Until Page Contains        Battery Usage